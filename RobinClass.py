#!/usr/bin/env python
# coding: utf-8

# # Define the Robin Boundary Conditions Class

# In[ ]:


from BoundaryClass import *


# In[ ]:


import numpy as np

class RobinBc:
    """Class defining a Robin boundary condition"""
    
    def __init__(self, phi, grid, ratio, Tinf, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            ratio ...... ratio of convection to conduction
            tinf........ Bulk temperature of convective fluid
            loc ........ boundary location
            
        """
        self._phi = phi
        self._grid = grid
        self._ratio = ratio #let ratio = h/k
        self._Tinf = Tinf #Let Tinf = T infinity
        self._loc = loc
        
    def value(self):
        """Return the boundary condition value"""
        if self._loc is BoundaryLocation.WEST:
            return (self._phi[1]+self._grid.dx_WP[0]*self._ratio*self._Tinf)/(1+self._grid.dx_WP[0]*self._ratio)
        elif self._loc is BoundaryLocation.EAST:                                                                       
            return (self._phi[-2]+self._grid.dx_PE[-1]*self._ratio*self._Tinf)/(1+self._grid.dx_PE[-1]*self._ratio)
        else:
            raise ValueError("Unknown boundary location")
    
    def coeff(self):
        """Return the linearization coefficient"""
        if self._loc is BoundaryLocation.WEST:
            return 1/(1+self._grid.dx_WP[0]*self._ratio)
        elif self._loc is BoundaryLocation.EAST:
            return 1/(1+self._grid.dx_PE[-1]*self._ratio)
        else:
            raise ValueError("Unknown boundary location")
    
    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            self._phi[0] = (self._phi[1]+self._grid.dx_WP[0]*self._ratio*self._Tinf)/(1+self._grid.dx_WP[0]*self._ratio)
        elif self._loc is BoundaryLocation.EAST:
            self._phi[-1] = (self._phi[-2]+self._grid.dx_PE[-1]*self._ratio*self._Tinf)/(1+self._grid.dx_PE[-1]*self._ratio)
        else:
            raise ValueError("Unknown boundary location")


# In[ ]:




