#!/usr/bin/env python
# coding: utf-8

# # Define the Boundary Locations Class

# In[1]:


import numpy as np

from enum import Enum

class BoundaryLocation(Enum):
    """Enumeration class defining boundary condition locations"""
    WEST = 1
    EAST = 2

