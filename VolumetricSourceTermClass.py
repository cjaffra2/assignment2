#!/usr/bin/env python
# coding: utf-8

# # Class to Define the Volumetric Source Term

# In[ ]:


import numpy as np

class VolumetricSourceTermModel:
    """Class defining a Volumetric Source Term"""
    
    def __init__(self, grid, heatgen):
        """Constructor"""
        self._grid = grid
        self._heatgen = heatgen #This value will be W/m^3
        
    def add(self, coeffs):
        """Function to add surface convection terms to coefficient arrays"""
        
        # Calculate the source term
        source = -self._heatgen*self._grid.vol
    
        # Calculate the linearization coefficients
        coeffP = 0
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(source)
        
        return coeffs

