#!/usr/bin/env python
# coding: utf-8

# In[1]:


class SecondOrderTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, Toldold, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Toldold = Toldold
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        Temporal_Flux = self._rho*self._cp*self._grid.vol*(1.5*self._T[1:-1]-2*self._Told[1:-1]+0.5*self._Toldold[1:-1])/self._dt
        
        # Calculate the linearization coefficient
        coeffP = 1.5*self._rho*self._cp*self._grid.vol/self._dt
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(Temporal_Flux)

        return coeffs


# In[ ]:




