#!/usr/bin/env python
# coding: utf-8

# # Define the Radiation Model Class

# In[2]:


from LinearizationTypeClass import *


# In[3]:


class RadiationModel:
    """Class defining a radiation model"""
    
    def __init__(self, grid, T, Trad, e, LinType):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Trad = Trad # T radiation
        self._e = e # Emissivity 
        self._LinType = LinType # Explicit or Implicit Linearization

        
    def add(self, coeffs):
        """Function to add surface convection terms to coefficient arrays"""
        
        # Calculate the source term
        source = self._e*5.67e-8*self._grid.Ao*(self._T[1:-1]**4 - self._Trad**4)
        
        # Calculate the linearization coefficients
        if self._LinType is LinearizationType.Implicit:
            coeffP = 4*self._e*5.67e-8*self._grid.Ao*self._T[1:-1]**3
        elif self._LinType is LinearizationType.Explicit:
            coeffP = 0
        else:
            raise ValueError("Linearization type must be Explicit or Implicit")
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(source)
        
        return coeffs


# In[ ]:




